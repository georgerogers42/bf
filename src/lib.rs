use std::io::prelude::*;
use std::io;

fn get_char(s: &str, i: usize) -> char {
    s[i..].chars().next().expect("Char should be here")
}

pub type Elt = u8;

pub struct Env { 
    pub posn: usize,
    pub data: Box<[Elt]>,
}

impl Env {
    pub fn new(size: usize) -> Env {
        let data = (0..size).map(|_| 0).collect::<Vec<Elt>>().into_boxed_slice();
        Env { posn: 0, data }
    }
    pub fn run(&mut self, prog: &str) {
        let mut stack = vec![];
        let stdin = io::stdin();
        let stdout = io::stdout();
        let mut pc = 0;
        while pc < prog.len() {
            let c = get_char(prog, pc);
            match c {
                '+' => {
                    if self.value() == Elt::max_value() {
                        self.data[self.posn] = 0;
                    } else {
                        self.data[self.posn] += 1;
                    }
                    pc += c.len_utf8();
                } '-' => {
                    if self.value() == 0 {
                        self.data[self.posn] = Elt::max_value();
                    } else {
                        self.data[self.posn] -= 1;
                    }
                    pc += c.len_utf8();
                } '>' => {
                    self.posn += 1;
                    if self.posn == self.data.len() {
                        self.posn = 0;
                    }
                    pc += c.len_utf8();
                } '<' => {
                    if self.posn == 0 {
                        self.posn = self.data.len() - 1;
                    } else {
                        self.posn -= 1;
                    }
                    pc += c.len_utf8();
                } ',' => {
                    let mut b = [0];
                    stdin.lock().read(&mut b).expect("Read Failed");
                    self.data[self.posn] = b[0] as Elt;
                    pc += c.len_utf8();
                } '.' => {
                    let b = [self.value() as u8];
                    stdout.lock().write_all(&b).expect("Write Failed");
                    pc += c.len_utf8();
                } '{' => {
                    pc += c.len_utf8();
                    if self.value() == 0 {
                        self.skip(&mut pc, prog, '}');
                    }
                } '[' => {
                    pc += c.len_utf8();
                    if self.value() == 0 {
                        self.skip(&mut pc, prog, ']');
                    } else {
                        stack.push(pc);
                    }
                } ']' => {
                    if self.value() == 0 {
                        stack.pop().expect("Unopened Loop");
                        pc += c.len_utf8();
                    } else {
                        pc = stack.last().map(|p| *p).expect("Unopened Loop");
                    }
                } '(' => {
                    pc += c.len_utf8();
                    self.comment(&mut pc, prog);
                } _ => {
                    pc += c.len_utf8();
                }
            }
        }
    }
    fn skip(&mut self, pc: &mut usize, prog: &str, end: char) {
        while *pc < prog.len() {
            let c = get_char(prog, *pc);
            match c {
                '{' => {
                    *pc += c.len_utf8();
                    self.skip(pc, prog, '}');
                } '[' => {
                    *pc += c.len_utf8();
                    self.skip(pc, prog, ']')
                } _ if c == end => {
                    *pc += c.len_utf8();
                    return;
                } '(' => {
                    *pc += c.len_utf8();
                    self.comment(pc, prog);
                } _ => {
                    *pc += c.len_utf8();
                }
            }
        }
    }
    fn comment(&mut self, pc: &mut usize, prog: &str) {
        while *pc < prog.len() {
            let c = get_char(prog, *pc);
            match c {
                '(' => {
                    *pc += c.len_utf8();
                    self.comment(pc, prog);
                } ')' => {
                    *pc += c.len_utf8();
                    return;
                } _ => {
                    *pc += c.len_utf8();
                }
            }
        }
    }
    pub fn value(&self) -> Elt {
        self.data[self.posn]
    }
}
