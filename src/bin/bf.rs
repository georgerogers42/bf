extern crate bf;
use std::io::prelude::*;
use std::io;
use std::fs;
use std::env;

fn main() {
    let argv: Vec<String> = env::args().collect();
    let mut env = bf::Env::new(1024*1024);
    let mut prog = String::new();
    {
        let mut f = io::BufReader::new(fs::File::open(&argv[1]).unwrap());
        f.read_to_string(&mut prog).unwrap();
    }
    env.run(&prog);
}
